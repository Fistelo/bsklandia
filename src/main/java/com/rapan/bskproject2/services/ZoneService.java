package com.rapan.bskproject2.services;

import com.rapan.bskproject2.entities.Zone;
import com.rapan.bskproject2.repositories.ZoneRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ZoneService {
    ZoneRepository zoneRepository;

    public ZoneService(ZoneRepository zoneRepository) {
        this.zoneRepository = zoneRepository;
    }

    public Zone getById(long zoneId){
        Optional<Zone> zone = zoneRepository.findById(zoneId);
        return zone.isPresent() ? zone.get() : null;
    }
}
