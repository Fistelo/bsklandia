package com.rapan.bskproject2.services;

import com.rapan.bskproject2.commands.EntertainerDTO;
import com.rapan.bskproject2.commands.ServiceworkDTO;
import com.rapan.bskproject2.entities.Entertainer;
import com.rapan.bskproject2.entities.ServiceWork;
import com.rapan.bskproject2.entities.User;
import com.rapan.bskproject2.entities.Zone;
import com.rapan.bskproject2.repositories.EntertainerRepository;
import com.rapan.bskproject2.repositories.ServiceWorkRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class EntertainerService {
    EntertainerRepository entertainerRepository;
    ServiceWorkRepository serviceWorkRepository;

    UserService userService;
    ZoneService zoneService;

    public EntertainerService(EntertainerRepository entertainerRepository, ServiceWorkRepository serviceWorkRepository, UserService userService, ZoneService zoneService) {
        this.entertainerRepository = entertainerRepository;
        this.serviceWorkRepository = serviceWorkRepository;
        this.userService = userService;
        this.zoneService = zoneService;
    }

    public void registerServiceWork(ServiceworkDTO dto){
        User overseer = userService.getUserByName(dto.getUserName());
        Optional<Entertainer> optionalEntertainer = entertainerRepository.findById(dto.getEntertainerId());
        if(optionalEntertainer.isPresent()){
            Entertainer entertainer = optionalEntertainer.get();
            ServiceWork serviceWork = new ServiceWork(dto.getTitle(), dto.getDescription(), LocalDate.now(), overseer, entertainer);
            entertainer.addServiceWork(serviceWork);
            entertainerRepository.save(entertainer);
            serviceWorkRepository.save(serviceWork);
        } else {
            log.error("There is no entertainer with provided id.");
        }
    }

    public List<Entertainer> getByZone(long zoneId){
        List<Entertainer> entertainers = entertainerRepository.findByZone(zoneId);
        return entertainers;
    }

    public Entertainer getById(long entertainerId){
        Optional<Entertainer> entertainer = entertainerRepository.findById(entertainerId);
        return entertainer.isPresent() ? entertainer.get() : null;
    }

    public long getZoneId(long entertainerId){
        Entertainer entertainer = this.getById(entertainerId);
        if (entertainer == null || entertainer.getZone() == null){
            log.error("Entertainer either null or without zone!");
            return 0;
        } else {
            return entertainer.getZone().getId();
        }
    }

    public long addEntertainer(EntertainerDTO dto) {
        Zone zone = zoneService.getById(dto.getZoneId());
        Entertainer entertainer = new Entertainer(dto.getName(), zone, dto.getMinHeight(), dto.getDescription());
        entertainerRepository.save(entertainer);
        return entertainer.getId();
    }

    public void deleteEntertainer (long id) {
        Entertainer entertainer = getById(id);
        if (entertainer != null) {
            entertainerRepository.delete(entertainer);
        } else {
            log.warn("You are trying to delete entertainer that does not exist.");
        }
    }
}
