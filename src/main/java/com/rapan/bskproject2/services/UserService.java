package com.rapan.bskproject2.services;

import com.rapan.bskproject2.entities.User;
import com.rapan.bskproject2.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUserByName(String username){
        Optional<User> user = userRepository.findByName(username);
        return user.isPresent() ? user.get() : null;
    }

}
