package com.rapan.bskproject2.services;

import com.rapan.bskproject2.commands.TicketDTO;
import com.rapan.bskproject2.commands.converters.TicketConverter;
import com.rapan.bskproject2.entities.Ticket;
import com.rapan.bskproject2.repositories.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TicketService {

    private final TicketRepository ticketRepository;
    private final TicketConverter ticketConverter;

    @Autowired
    public TicketService(TicketRepository ticketRepository, TicketConverter ticketConverter) {
        this.ticketRepository = ticketRepository;
        this.ticketConverter = ticketConverter;
    }

    public void addTicket(TicketDTO ticketDto) {
        Ticket ticket = ticketConverter.convert(ticketDto);
        ticketRepository.save(ticket);
    }

    public List<Ticket> getTicketsByName(String username){
        return ticketRepository.getTicketsByUserName(username);
    }

    public void deleteTicket() {

    }
}
