package com.rapan.bskproject2.security;

import com.rapan.bskproject2.entities.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomUserDetailsService userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.
                authorizeRequests()
                    .antMatchers("/bilety").hasAuthority(Role.USER.toString())
                    .antMatchers("/atrakcje/addwork").hasAuthority(Role.SERVICEMAN.toString())
                    .antMatchers("/atrakcje/add").hasAuthority(Role.ADMIN.toString())
                    .antMatchers("/admin").hasAuthority(Role.ADMIN.toString())
                .and().formLogin()
                    .loginPage("/login")
                    .successForwardUrl("/index")
                .and().logout()
                    .logoutUrl("/logout")
                 //   .logoutSuccessUrl("/index")

                /*TODO: Delete it after debug phase*/
                .and().csrf().disable()
                .headers().frameOptions().disable();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider(){
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(new BCryptPasswordEncoder());
        provider.setUserDetailsService(userDetailsService);
        return provider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }
}
