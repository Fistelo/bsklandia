package com.rapan.bskproject2.commands;

import com.rapan.bskproject2.entities.User;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;


@Data
public class TicketDTO {
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;
    private TicketType type;
    private String note;
    private User user;
}
