package com.rapan.bskproject2.commands;

public enum TicketType {
    NORMAL, DISCOUNTED
}
