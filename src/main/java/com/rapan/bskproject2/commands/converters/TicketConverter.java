package com.rapan.bskproject2.commands.converters;

import com.rapan.bskproject2.commands.TicketDTO;
import com.rapan.bskproject2.entities.Ticket;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class TicketConverter implements Converter<TicketDTO, Ticket> {
    @Nullable
    @Override
    public Ticket convert(TicketDTO source) {
        if(source == null)
            return null;
        com.rapan.bskproject2.entities.Ticket ticket = new com.rapan.bskproject2.entities.Ticket();
        ticket.setType(source.getType());
        ticket.setDate(source.getDate());
        ticket.setNote(source.getNote());
        ticket.setUser(source.getUser());
        return ticket;
    }
}
