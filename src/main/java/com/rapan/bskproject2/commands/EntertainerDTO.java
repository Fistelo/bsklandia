package com.rapan.bskproject2.commands;

import lombok.Data;

@Data
public class EntertainerDTO {
    private String name;
    private int minHeight;
    private String description;
    private long zoneId;
}
