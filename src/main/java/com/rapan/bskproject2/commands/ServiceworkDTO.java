package com.rapan.bskproject2.commands;

import com.rapan.bskproject2.entities.Entertainer;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ServiceworkDTO {
    private String title;
    private String description;
    private long entertainerId;
    private String userName;
}
