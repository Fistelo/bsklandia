package com.rapan.bskproject2.repositories;

import com.rapan.bskproject2.entities.Zone;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ZoneRepository extends CrudRepository<Zone, Long> {
    Optional<Zone> findById(@Param("zoneId") long id);
}
