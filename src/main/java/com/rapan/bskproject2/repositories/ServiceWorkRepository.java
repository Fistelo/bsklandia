package com.rapan.bskproject2.repositories;

import com.rapan.bskproject2.entities.ServiceWork;
import org.springframework.data.repository.CrudRepository;

public interface ServiceWorkRepository extends CrudRepository<ServiceWork, Long> {
}
