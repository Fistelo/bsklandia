package com.rapan.bskproject2.repositories;

import com.rapan.bskproject2.entities.Ticket;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface TicketRepository extends CrudRepository<Ticket, Long>{

    @Query("select t from Ticket t join t.user u where u.name = :name")
    List<Ticket> getTicketsByUserName(@Param("name") String name);
}
