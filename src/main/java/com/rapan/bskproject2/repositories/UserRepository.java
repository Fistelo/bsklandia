package com.rapan.bskproject2.repositories;

import com.rapan.bskproject2.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findByName(@Param("name") String name);
}
