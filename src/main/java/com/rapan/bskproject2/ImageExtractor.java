package com.rapan.bskproject2;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

public class ImageExtractor {

    public static byte[] extractBytesFromImage(File img) throws IOException{
        BufferedImage bufferedImage = ImageIO.read(img);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "png", outputStream);
        outputStream.flush();
        byte[] bytes = outputStream.toByteArray();
        outputStream.close();

        return bytes;
    }

    public static BufferedImage extractImageFromBytes(byte[] bytes){
        InputStream in = new ByteArrayInputStream(bytes);
        BufferedImage image = null;
        try {
            image = ImageIO.read(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }
}
