package com.rapan.bskproject2.controllers;

import com.rapan.bskproject2.commands.TicketDTO;
import com.rapan.bskproject2.entities.Ticket;
import com.rapan.bskproject2.services.TicketService;
import com.rapan.bskproject2.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.List;

@Controller
public class TicketController {

    private final TicketService ticketService;
    private final UserService userService;

    @Autowired
    public TicketController(TicketService ticketService, UserService userService) {
        this.ticketService = ticketService;
        this.userService = userService;
    }

    @RequestMapping("/bilety")
    public String getTickets(Principal principal, ModelMap model){
        List<Ticket> tickets = ticketService.getTicketsByName(principal.getName());
        model.addAttribute("userTickets", tickets);
        return "bilety";
    }

    @PostMapping("/bilety")
    public String reserveTicket(@ModelAttribute TicketDTO ticket, Principal principal){
        ticket.setUser(userService.getUserByName(principal.getName()));
        ticketService.addTicket(ticket);
        return "redirect:bilety";
    }

    @PostMapping("/bilety/{id}/remove")
    public void removeTicket(@PathVariable("id") long id){

    }

}
