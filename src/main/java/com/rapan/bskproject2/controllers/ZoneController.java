package com.rapan.bskproject2.controllers;

import com.rapan.bskproject2.commands.EntertainerDTO;
import com.rapan.bskproject2.commands.ServiceworkDTO;
import com.rapan.bskproject2.entities.Zone;
import com.rapan.bskproject2.repositories.ZoneRepository;
import com.rapan.bskproject2.services.EntertainerService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
public class ZoneController {

    private ZoneRepository zoneRepository;
    private EntertainerService entertainerService;

    public ZoneController(ZoneRepository zoneRepository, EntertainerService entertainerService) {
        this.zoneRepository = zoneRepository;
        this.entertainerService = entertainerService;
    }

    @RequestMapping("/strefy")
    public String setZonesPage(Model model){
        model.addAttribute("allZones", zoneRepository.findAll());
        return "strefy";
    }

    @RequestMapping("/atrakcje")
    public String setEntertainersPage(@RequestParam long zoneId, Model model){
        model.addAttribute("zoneEntertainers", entertainerService.getByZone(zoneId));
        Optional<Zone> zone = zoneRepository.findById(zoneId);
        model.addAttribute("zoneName", zone.isPresent() ? zone.get().getName() : "Zone");
        model.addAttribute("zoneId", zone.isPresent() ? zone.get().getId() : 0);
        return "atrakcje";
    }

    @PostMapping("/atrakcje/addwork")
    public String addWord(@ModelAttribute ServiceworkDTO serviceWorkDTO){
        entertainerService.registerServiceWork(serviceWorkDTO);
        long zoneId = entertainerService.getZoneId(serviceWorkDTO.getEntertainerId());
        return "redirect:/atrakcje?zoneId=" + zoneId ;
    }

    @GetMapping("/atrakcje/remove/{id}")
    public String deleteAttraction(@PathVariable("id") long id){
        long zoneId = entertainerService.getZoneId(id);
        entertainerService.deleteEntertainer(id);
        return "redirect:/atrakcje?zoneId=" + zoneId;
    }

    @PostMapping("/atrakcje")
    public String addAttraction(@ModelAttribute EntertainerDTO entertainer){
        long entertainerId = entertainerService.addEntertainer(entertainer);
        long zoneId = entertainerService.getZoneId(entertainerId);
        return "redirect:/atrakcje?zoneId=" + zoneId;
    }
}
