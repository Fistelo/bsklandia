package com.rapan.bskproject2.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @RequestMapping(value = {"/", "/home", "/index", ""})
    public String getHomePage(){

        return "index";
    }

}
