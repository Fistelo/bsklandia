package com.rapan.bskproject2.controllers;

import com.rapan.bskproject2.commands.UserDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class SecurityController {

    @RequestMapping("/login")
    public String getLoginPage(){
        return "login";
    }

}
