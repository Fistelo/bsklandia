package com.rapan.bskproject2;

import com.rapan.bskproject2.commands.TicketType;
import com.rapan.bskproject2.entities.*;
import com.rapan.bskproject2.repositories.*;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Arrays;

@Component
public final class Bootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private EntertainerRepository entertainerRepository;
    private ZoneRepository zoneRepository;
    private UserRepository userRepository;
    private ServiceWorkRepository serviceWorkRepository;
    private TicketRepository ticketRepository;
    private BCryptPasswordEncoder bCryptEncoder;

    public Bootstrap(EntertainerRepository entertainerRepository, ZoneRepository zoneRepository,
                     UserRepository userRepository, ServiceWorkRepository serviceWorkRepository,
                     TicketRepository ticketRepository) {
        this.entertainerRepository = entertainerRepository;
        this.zoneRepository = zoneRepository;
        this.userRepository = userRepository;
        this.serviceWorkRepository = serviceWorkRepository;
        this.ticketRepository = ticketRepository;
        bCryptEncoder = new BCryptPasswordEncoder();
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        fillDatabase();
    }

    private void fillDatabase() {
        Zone zone1 = new Zone("Familijna");
        Zone zone2 = new Zone("Ekstremalna");
        Zone zone3 = new Zone("Bajkolandia");

        Entertainer e1 = new Entertainer("Happy Loops", zone3, 100, "Kolejka pełna kolorów, radości i wyśmienitej zabawy dla każdego. Trasa pełna zakrętów, zwrotów, w kilku miejscach solidne przyspieszenia – słowem frajda na całego!");
        Entertainer e2 = new Entertainer("Karuzela Funny Cars", zone3, 100, "Radosna, kolorowa karuzela dla najmłodszych Gości. Maluszki, szczególnie płci męskiej mogą spełnić swoje marzenia o samodzielnym prowadzeniu pojazdu!\n");
        Entertainer e3 = new Entertainer("Arctic Fun", zone3, 100, "To kolejne urządzenie, które choć znajduje się w strefie dziecięcej, idealnie nadaje się także dla dorosłych, pragnących bawić się wraz ze swoimi pociechami. Utrzymana w arktycznych klimatach, stanowi wariację wszystkich znanych zjeżdżalni. Duża wysokość pozwala cieszyć się jazdą, a specjalne materace nie tylko chronią nogi, ale również sprawiają, że zjazd jest o wiele szybszy.");
        Entertainer e4 = new Entertainer("Space Booster", zone2, 140, "Ekstremalne urządzenie największego parku rozrywki w Polsce w Zatorze. Podstawa tego urządzenia to ogromne ramię o średnicy 40 metrów poruszające się z niesamowitą prędkością dochodzącą do 100 km/h. i generujące przeciążenie prawie 4g. Wyposażone jest w dwie czteroosobowe kapsuły, zatem jednorazowo zabiera 8 osób, które doświadczając siły bezwładności obracają się wokół swojej osi.");
        Entertainer e5 = new Entertainer("Mayan", zone2, 140, "Trzeci, najbardziej ekstremalny Roller-Coaster „Mayan” jest to największe tego typu urządzenie w Europie Środkowo-Wschodniej znajduje się nieopodal wysokiej na 40 metrów wieży Tsunami Drop oraz jednej z najbardziej ekstremalnych atrakcji Aztec Swing. Jeśli oceniać roller costery według zaawansowania technicznego, wielkości i oferowanych wrażeń oraz prestiżu? Głodni naprawdę mocnych wrażeń goście naszego Parku będą mogli je zaspokoić właśnie w tym miejscu. Prędkość jaką osiągają wagoniki na tej kolejce to 80 km/h, natomiast aż 5 inwersji sprawi, że przeciążenia dochodzić będą do prawie 5g!!!");
        Entertainer e6 = new Entertainer("Space Gun", zone2, 140, "W strefie ekstremalnej największego parku rozrywki w Polsce Energylandia to atrakcja składająca się z dwóch 16-osobowych gondoli, które umieszczone są na potężnych 16 metrowych ramionach. Poruszające się w przeciwnych kierunkach z oszałamiającą prędkością generują ogromne przeciążenia. Oryginalne jest to, że atrakcja nie posiada zadaszenia co gwarantuje większy poziom emocji. Jest to jedyna atrakcja która nie posiada automatycznego trybu pracy, każda więc przejażdżka jest zupełnie inna!");
        Entertainer e7 = new Entertainer("Viking Ride", zone1, 120, "Ekscytująca podróż łodzią stylizowaną na dawne tratwy. Trasa wkomponowana jest w Wioskę Wikingów w pobliżu Roller Coastera Dragona, z licznymi niespodziankami i atrakcjami, co jeszcze bardziej potęguje wrażenia podczas spływu.");
        Entertainer e8 = new Entertainer("Anaconda", zone1, 120, "To emocjonująca przejażdżka ogromnymi, dwudziestoosobowymi łodziami po trasie pełnej mokrych niespodzianek. ;)Strome wodospady rozpędzają wagoniki do prędkości 55 km/h aby z dużym impetem uderzyć w taflę wody i wywołać spektakularną falę Gwarantujemy, że z tej przejażdżki nikt nie wyjdzie na sucho!");
        Entertainer e9 = new Entertainer("Boomerang", zone1, 120, "atrakcja uniwersalna – zapewniającą zarówno, fajne ekstremalne momenty z dreszczykiem ale w „rozsądnej dawce” ;) – dostępna jest dla szerszego grona – począwszy od dzieci poprzez młodzież na dorosłych skończywszy. I istotne – Boomerang to podwójna radość ponieważ atrakcja porusza się do przodu i do tyłu");

        zone1.getEntertainers().addAll(Arrays.asList(e7, e8, e9));
        zone2.getEntertainers().addAll(Arrays.asList(e4, e5, e6));
        zone3.getEntertainers().addAll(Arrays.asList(e1, e2, e3));

        User user1 = new User("User", bCryptEncoder.encode("user"));
        user1.getRoles().add(Role.USER);
        User user2 = new User("Service", bCryptEncoder.encode("service"));
        user2.getRoles().add(Role.SERVICEMAN);
        User user3 = new User("Admin", bCryptEncoder.encode("admin"));
        user3.getRoles().add(Role.ADMIN);

        Ticket ticket1 = new Ticket();
        ticket1.setNote("nowy bilet");
        ticket1.setUser(user1);
        ticket1.setDate(LocalDate.now());
        ticket1.setType(TicketType.NORMAL);
        user1.setTicket(Arrays.asList(ticket1));

        ServiceWork serviceWork1 = new ServiceWork("Czyszczenie", "Wyczyscilem elegancko",
                LocalDate.of(2017, 10, 2), user2, e1);
        user2.getServiceWorks().add(serviceWork1);

        userRepository.save(user1);
        ticketRepository.save(ticket1);
        userRepository.save(user2);
        userRepository.save(user3);
        zoneRepository.saveAll(Arrays.asList(zone1, zone2, zone3));
        entertainerRepository.saveAll(Arrays.asList(e1, e2, e3, e4, e5, e6, e7, e8, e9));
        serviceWorkRepository.save(serviceWork1);
    }
}
