package com.rapan.bskproject2.entities;


import com.rapan.bskproject2.commands.TicketType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Getter @Setter
    private LocalDate date;
    @Getter @Setter
    private TicketType type;
    @Getter @Setter
    private String note;
    @ManyToOne
    @Getter @Setter
    User user;
}
