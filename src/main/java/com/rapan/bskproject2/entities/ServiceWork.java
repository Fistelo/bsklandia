package com.rapan.bskproject2.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@NoArgsConstructor
public class ServiceWork {
    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Getter @Setter
    private String title;
    @Getter @Setter
    private String description;
    @Getter @Setter
    private LocalDate date;
    @ManyToOne
    @Getter @Setter
    private User overseer;
    @ManyToOne
    @Getter @Setter
    private Entertainer entertainer;

    public ServiceWork(String title, String description, LocalDate date, User overseer, Entertainer entertainer) {
        this.title = title;
        this.description = description;
        this.date = date;
        this.overseer = overseer;
        this.entertainer = entertainer;
    }
}
