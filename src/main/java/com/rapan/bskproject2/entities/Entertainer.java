package com.rapan.bskproject2.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
public class Entertainer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter
    private Long id;
    @Getter @Setter
    private String name;
    @Getter @Setter
    private int minHeight;
    @ManyToOne
    @Getter @Setter
    private Zone zone;
    @Lob
    @Getter @Setter
    private String description;

    @OneToMany(mappedBy = "entertainer")
    @Getter @Setter
    private List<ServiceWork> serviceWorks = new ArrayList<>();

    public Entertainer(String name, Zone zone, int minHeight, String description) {
        this.description = description;
        this.name = name;
        this.zone = zone;
        this.minHeight = minHeight;
    }

    public void addServiceWork(ServiceWork serviceWork){
        this.serviceWorks.add(serviceWork);
        serviceWork.setEntertainer(this);
    }
}
