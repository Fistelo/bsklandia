package com.rapan.bskproject2.entities;

public enum Role {
    USER, ADMIN, SERVICEMAN
}
