package com.rapan.bskproject2.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@EqualsAndHashCode(exclude = "entertainers")
@NoArgsConstructor
public class Zone {

    @Id
    @Getter @Setter
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Getter @Setter
    private String name;

    @Getter @Setter
    @OneToMany(mappedBy = "zone")
    private Set<Entertainer> entertainers = new HashSet<>();

    public Zone(String name) {
        this.name = name;
    }
}