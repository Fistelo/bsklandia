package com.rapan.bskproject2.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@EqualsAndHashCode(exclude = "roles, serviceWorks")
@NoArgsConstructor
public class User {

    @Id
    @Getter @Setter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Getter @Setter
    private String name;
    @Getter @Setter
    private String password;

    @Getter @Setter
    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
    @Enumerated(value = EnumType.STRING)
    private Set<Role> roles = new HashSet<>();

    @OneToMany(mappedBy = "overseer")
    @Getter @Setter
    private List<ServiceWork> serviceWorks = new ArrayList<>();

    @OneToMany(mappedBy = "user")
    @Getter @Setter
    List<Ticket> ticket;

    public User(String _name, String _password){
        this.name = _name;
        this.password = _password;
    }
}
